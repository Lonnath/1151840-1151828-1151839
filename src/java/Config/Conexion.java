
package Config;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class Conexion {

    public DriverManagerDataSource connect() {
        DriverManagerDataSource data = new DriverManagerDataSource();
        data.setDriverClassName("com.mysql.jdbc.Driver");
        data.setUrl("jdbc:mysql://localhost:3306/lumier_db"); // dbname = nombre de la base de datos
        data.setUsername("root"); // root by default
        data.setPassword("1996"); // clave de root en la bd 
        return data;
    }
}
