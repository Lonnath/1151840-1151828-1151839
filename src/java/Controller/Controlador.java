/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Config.Conexion;
import Entidad.Cuenta;
import Entidad.Libro;
import Entidad.Persona;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class Controlador {

    Conexion conexion = new Conexion();
    JdbcTemplate jdbcTemplate = new JdbcTemplate(conexion.connect());
    ModelAndView mav = new ModelAndView();
    Cuenta cuentaTemp = null;
    @RequestMapping("Estanteria.htm")
    public ModelAndView estanteria() {
        String consulta = "select * from ofertas";
        List query = this.jdbcTemplate.queryForList(consulta);
        ModelAndView modelo = new ModelAndView("Estanteria");
        String menu = "<li class=\"nav-item dropdown tamanio-drop\">\n"
                + "                    <a class=\"nav-link\" href=\"index.htm\">\n"
                + "                        <div class=\"text-title navi font-cairo\">Iniciar Sesión</div>\n"
                + "                    </a>\n"
                + "                </li>";
        String oferta = "<button disabled>OFERTA</button>";
        String solicitud = "<button disabled>SOLICITUD</button>";
        if (this.cuentaTemp != null) {
            menu = "<li class=\"nav-item dropdown tamanio-drop\"><a class=\"nav-link\" href=\"#\" id=\"navbarDropdownMenuLink\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"><div class=\"text-title dropdown-toggle navi font-cairo\">Cuenta</div></a><div class=\"drop text-center tamanio-drop\"><div class=\"dropdown-menu tamanio-drop\" aria-labelledby=\"navbarDropdownMenuLink\"><a class=\"dropdown-item\" href=\"perfil.htm\">Perfil</a><a class=\"dropdown-item\" href=\"carrito.htm\">Carrito</a><a class=\"dropdown-item\" href=\"config.htm\">Configuración</a><div class=\"dropdown-divider\"></div><a class=\"dropdown-item\" href=\"cerrar.htm\">Cerrar Sesión</a></div></div></li>";
            oferta = "<a class=\"btn btn-info\" href=\"oferta.htm\" >OFERTA</a>";
            solicitud = "<a class=\"btn btn-danger\" href=\"solicitud.htm\" >SOLICITUD</a>";
        }
        modelo.addObject("info", query);
        modelo.addObject("oferta", oferta);
        modelo.addObject("solicitud", solicitud);
        modelo.addObject("menu", menu);
        return modelo;
    }

    

    @RequestMapping("principal.htm")
    public ModelAndView principal() {
        ModelAndView modelo = new ModelAndView("redirect:/principal.htm");
        String menu = "<li class=\"nav-item dropdown tamanio-drop\">\n"
                + "                    <a class=\"nav-link\" href=\"#\" id=\"navbarDropdownMenuLink\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n"
                + "                        <div class=\"text-title dropdown-toggle navi font-cairo\">Iniciar Sesión</div>\n"
                + "                    </a>\n"
                + "                    <div class=\"drop text-center tamanio-drop\">\n"
                + "                        <div class=\"dropdown-menu tamanio-drop text-center shadow\" aria-labelledby=\"navbarDropdownMenuLink\">\n"
                + "                            <form class=\"tamanio-drop\" method=\"POST\">\n"
                + "                                <div class=\"form-group\">\n"
                + "                                    <label for=\"inputUser\" class=\"font-exo2\">Nombre de Usuario</label>\n"
                + "                                    <div class=\"dropdown-divider\"></div>\n"
                + "                                    <input type=\"text\" class=\"form-control tamanio-text-box-login m-4\" id=\"inputUser\" name=\"nombreUsuario\" aria-describedby=\"emailHelp\" placeholder=\"Ingresar Username\">\n"
                + "                                </div>\n"
                + "                                <div class=\"form-group\">\n"
                + "                                    <label for=\"inputPassword\" class=\"font-exo2\">Contraseña</label>\n"
                + "                                    <div class=\"dropdown-divider\"></div>\n"
                + "                                    <input type=\"password\" class=\"form-control tamanio-text-box-login m-4\" name=\"contrasenia\" placeholder=\"********\">\n"
                + "                                    <small id=\"emailHelp\" class=\"form-text text-muted\">Nuestro equipo nunca le solicitará información personal, recuerde proteger su información.</small>\n"
                + "                                </div>\n"
                + "                                <div class=\"form-group form-check\">\n"
                + "                                    <input type=\"checkbox\" class=\"form-check-input\" id=\"checkPermanecer\" name=\"checkPermanecer\">\n"
                + "                                    <label class=\"form-check-label\" for=\"exampleCheck1\">Permanecer conectado</label>\n"
                + "                                </div>\n"
                + "                                <button type=\"submit\" class=\"btn btn-primary \" value=\"login1\">INGRESAR</button>\n"
                + "                                <a type=\"button\" class=\"btn btn-secondary\" href=\"registrar.htm\">\n"
                + "                                    REGISTRAR\n"
                + "                                </a>\n"
                + "                            </form>\n"
                + "                        </div>\n"
                + "\n"
                + "                    </div>\n"
                + "                </li>";

        if (this.cuentaTemp != null) {
            menu = "<li class=\"nav-item dropdown tamanio-drop\">\n"
                    + "                    <a class=\"nav-link\" href=\"#\" id=\"navbarDropdownMenuLink\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n"
                    + "                        <div class=\"text-title dropdown-toggle navi font-cairo\">Cuenta</div>\n"
                    + "                    </a>\n"
                    + "                    <div class=\"drop text-center tamanio-drop\">\n"
                    + "                        <div class=\"dropdown-menu tamanio-drop\" aria-labelledby=\"navbarDropdownMenuLink\">\n"
                    + "                            <a class=\"dropdown-item\" href=\"perfil.htm\">Perfil</a>\n"
                    + "                            <a class=\"dropdown-item\" href=\"carrito.htm\">Carrito</a>\n"
                    + "                            <a class=\"dropdown-item\" href=\"config.htm\">Configuración</a>\n"
                    + "                            <div class=\"dropdown-divider\"></div>\n"
                    + "                            <a class=\"dropdown-item\" href=\"cerrar.htm\">Cerrar Sesión</a>\n"
                    + "                        </div>\n"
                    + "                    </div>\n"
                    + "                </li>";
        }
        modelo.addObject("menu", menu);
        return modelo;
    }

    @RequestMapping("carrito.htm")
    public ModelAndView carrito() {
        String consulta = "select * from compras where propietario =" + cuentaTemp.getNombreUsuario();
        List query = this.jdbcTemplate.queryForList(consulta);
        ModelAndView modelo = new ModelAndView("carrito");
        String menu = "<li class=\"nav-item dropdown tamanio-drop\">\n"
                + "                    <a class=\"nav-link\" href=\"index.htm\">\n"
                + "                        <div class=\"text-title navi font-cairo\">Iniciar Sesión</div>\n"
                + "                    </a>\n"
                + "                </li>";

        if (this.cuentaTemp != null) {
            menu="<li class=\"nav-item dropdown tamanio-drop\">\n"
                    + "                    <a class=\"nav-link\" href=\"#\" id=\"navbarDropdownMenuLink\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n"
                    + "                        <div class=\"text-title dropdown-toggle navi font-cairo\">Cuenta</div>\n"
                    + "                    </a>\n"
                    + "                    <div class=\"drop text-center tamanio-drop\">\n"
                    + "                        <div class=\"dropdown-menu tamanio-drop\" aria-labelledby=\"navbarDropdownMenuLink\">\n"
                    + "                            <a class=\"dropdown-item\" href=\"perfil.htm\">Perfil</a>\n"
                    + "                            <a class=\"dropdown-item\" href=\"carrito.htm\">Carrito</a>\n"
                    + "                            <a class=\"dropdown-item\" href=\"config.htm\">Configuración</a>\n"
                    + "                            <div class=\"dropdown-divider\"></div>\n"
                    + "                            <a class=\"dropdown-item\" href=\"cerrar.htm\">Cerrar Sesión</a>\n"
                    + "                        </div>\n"
                    + "                    </div>\n"
                    + "                </li>";
        }
        modelo.addObject("info", query);

        modelo.addObject("menu", menu);
        return modelo;
    }
    @RequestMapping("index.htm")
    public ModelAndView index() {
        ModelAndView nue = new ModelAndView("redirect:/index.htm");
        nue.addObject(new Object());
        return nue;
    }
    
    @RequestMapping("perfil.htm")
    public ModelAndView perfil() {
        return new ModelAndView("perfil");
    }

    @RequestMapping("config.htm")
    public ModelAndView config() {
        return new ModelAndView("config");
    }

    @RequestMapping("registrar.htm")
    public ModelAndView registrar() {
        return new ModelAndView("registrar");
    }

    
    @RequestMapping("cerrar.htm")
    public ModelAndView cerrar() {
        this.cuentaTemp = null;
        this.mav = new ModelAndView();
        return  new ModelAndView("redirect:/index.htm");

    }

    @RequestMapping(value = "index.htm", method = RequestMethod.GET)
    public ModelAndView Login() {
        mav.addObject(new Persona());
        return mav;
    }

    @RequestMapping(value = "index.htm", method = RequestMethod.POST)
    public ModelAndView Login(Cuenta cuenta) {
        String query = "select propietario from usuarios where usuario=? and contrasenia=" + cuenta.getContrasenia();
        int cantidad = 0;
        try {
            cantidad = this.jdbcTemplate.queryForObject(query, new Object[]{cuenta.getNombreUsuario()}, Integer.class);
        } catch (Exception e) {

        }
        if (cantidad > 0) {
            mav = new ModelAndView();
            this.cuentaTemp = cuenta;
            return new ModelAndView("redirect:/Estanteria.htm");
        } else {
            return new ModelAndView("redirect:/index.htm");
        }
    }

    @RequestMapping(value = "principal.htm", method = RequestMethod.GET)
    public ModelAndView Login1() {
        mav.addObject(new Persona());
        return mav;
    }

    @RequestMapping(value = "principal.htm", method = RequestMethod.POST)
    public ModelAndView Login1(Cuenta cuenta) {
        String query = "select propietario from usuarios where usuario=? and contrasenia=" + cuenta.getContrasenia();
        int cantidad = 0;
        try {
            cantidad = this.jdbcTemplate.queryForObject(query, new Object[]{cuenta.getNombreUsuario()}, Integer.class);
        } catch (Exception e) {

        }
        if (cantidad > 0) {
            mav = new ModelAndView();
            this.cuentaTemp = cuenta;
            return new ModelAndView("redirect:/Estanteria.htm");
        } else {
            return new ModelAndView("redirect:/index.htm");
        }
    }

    @RequestMapping(value = "registrar.htm", method = RequestMethod.GET)
    public ModelAndView registrarUsuario() {
        ModelAndView nuevo = new ModelAndView("registrar");
        nuevo.addObject(new Persona());
        return nuevo;
    }

    @RequestMapping(value = "registrar.htm", method = RequestMethod.POST)
    public ModelAndView registrarUsuario(Persona persona) {

        String registro = "insert into personas(id, nombre, documento_identidad, fecha_nacimiento, correo, departamento, ciudad, tarjeta_credito, fecha_expiracion)values(?,?,?,?,?,?,?,?,?)";
        this.jdbcTemplate.update(registro, null, persona.getNombre(), persona.getDocumento(), persona.getFecha(), persona.getCorreo(), persona.getDepartamento(), persona.getCiudad(), persona.getTarjeta(), persona.getFechaExpiracionTarjeta());
        String user = "insert into usuarios(id, usuario, contrasenia, propietario) values(?,?,?,?)";
        this.jdbcTemplate.update(user, null, persona.getUsername(), persona.getContrasenia(), persona.getDocumento());

        return new ModelAndView("redirect:/index.htm");

    }


    @RequestMapping(value = "solicitud.htm", method = RequestMethod.GET)
    public ModelAndView registrarSolicitud() {
        mav.addObject(new Libro());
        return mav;
    }

    @RequestMapping(value = "solicitud.htm", method = RequestMethod.POST)
    public void registrarSolicitud(Libro libro) {

        String registro = "insert into solicitudes(id, titulo, autor, anio_Publicacion, genero, propietario)values(?,?,?,?,?,?)";
        this.jdbcTemplate.update(registro, null, libro.getTitulo(), libro.getAutor(), libro.getAnioPublicacion(), libro.getGenero(), cuentaTemp.getNombreUsuario());
        mav = new ModelAndView();
        
    }
     @RequestMapping(value = "oferta.htm", method = RequestMethod.GET)
    public ModelAndView registrarOferta() {
        mav.addObject(new Libro());
        return mav;
    }

    @RequestMapping(value = "oferta.htm", method = RequestMethod.POST)
    public void registrarOferta(Libro libro) {

        String registro = "insert into solicitudes(id, titulo, autor, anio_Publicacion, imgDir, genero, propietario)values(?,?,?,?,?,?,?)";
        this.jdbcTemplate.update(registro, null, libro.getTitulo(), libro.getAutor(), libro.getAnioPublicacion(), "    ", libro.getGenero(), cuentaTemp.getNombreUsuario());
        mav = new ModelAndView();
        
    }

}
