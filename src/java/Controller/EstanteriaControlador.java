/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Config.Conexion;
import Entidad.Cuenta;
import Entidad.Libro;
import Entidad.Persona;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class EstanteriaControlador {

    Conexion conexion = new Conexion();
    JdbcTemplate jdbcTemplate = new JdbcTemplate(conexion.connect());
    ModelAndView mav = new ModelAndView();
    Cuenta cuentaTemp = null;


    @RequestMapping("Estanteria.htm")
    public ModelAndView estanteria() {
        String consulta = "select * from ofertas";
        List query = this.jdbcTemplate.queryForList(consulta);
        ModelAndView modelo = new ModelAndView("Estanteria");
        String menu = "<li class=\"nav-item dropdown tamanio-drop\">\n"
                + "                    <a class=\"nav-link\" href=\"index.htm\">\n"
                + "                        <div class=\"text-title navi font-cairo\">Iniciar Sesión</div>\n"
                + "                    </a>\n"
                + "                </li>";
        String oferta = "<button disabled>OFERTA</button>";
        String solicitud = "<button disabled>SOLICITUD</button>";
        if (this.cuentaTemp != null) {
            menu = "<li class=\"nav-item dropdown tamanio-drop\"><a class=\"nav-link\" href=\"#\" id=\"navbarDropdownMenuLink\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"><div class=\"text-title dropdown-toggle navi font-cairo\">Cuenta</div></a><div class=\"drop text-center tamanio-drop\"><div class=\"dropdown-menu tamanio-drop\" aria-labelledby=\"navbarDropdownMenuLink\"><a class=\"dropdown-item\" href=\"perfil.htm\">Perfil</a><a class=\"dropdown-item\" href=\"carrito.htm\">Carrito</a><a class=\"dropdown-item\" href=\"config.htm\">Configuración</a><div class=\"dropdown-divider\"></div><a class=\"dropdown-item\" href=\"cerrar.htm\">Cerrar Sesión</a></div></div></li>";
            oferta = "<a class=\"btn btn-info\" href=\"oferta.htm\" disabled>OFERTA</a>";
            solicitud = "<a class=\"btn btn-danger\" href=\"solicitud.htm\" disabled>SOLICITUD</a>";
        }
        modelo.addObject("info", query);
        modelo.addObject("oferta", oferta);
        modelo.addObject("solicitud", solicitud);
        modelo.addObject("menu", menu);
        return modelo;
    }

}
