/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Config.Conexion;
import Entidad.Cuenta;
import Entidad.Libro;
import Entidad.Persona;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class carritoControlador {

    Conexion conexion = new Conexion();
    JdbcTemplate jdbcTemplate = new JdbcTemplate(conexion.connect());
    ModelAndView mav = new ModelAndView();
    Cuenta cuentaTemp = null;

    @RequestMapping("carrito.htm")
    public ModelAndView carrito() {
        String consulta = "select * from compras where propietario =" + cuentaTemp.getNombreUsuario();
        List query = this.jdbcTemplate.queryForList(consulta);
        ModelAndView modelo = new ModelAndView("carrito");
        String menu = "<li class=\"nav-item dropdown tamanio-drop\">\n"
                + "                    <a class=\"nav-link\" href=\"index.htm\">\n"
                + "                        <div class=\"text-title navi font-cairo\">Iniciar Sesión</div>\n"
                + "                    </a>\n"
                + "                </li>";

        if (this.cuentaTemp != null) {
            menu="<li class=\"nav-item dropdown tamanio-drop\">\n"
                    + "                    <a class=\"nav-link\" href=\"#\" id=\"navbarDropdownMenuLink\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n"
                    + "                        <div class=\"text-title dropdown-toggle navi font-cairo\">Cuenta</div>\n"
                    + "                    </a>\n"
                    + "                    <div class=\"drop text-center tamanio-drop\">\n"
                    + "                        <div class=\"dropdown-menu tamanio-drop\" aria-labelledby=\"navbarDropdownMenuLink\">\n"
                    + "                            <a class=\"dropdown-item\" href=\"perfil.htm\">Perfil</a>\n"
                    + "                            <a class=\"dropdown-item\" href=\"carrito.htm\">Carrito</a>\n"
                    + "                            <a class=\"dropdown-item\" href=\"config.htm\">Configuración</a>\n"
                    + "                            <div class=\"dropdown-divider\"></div>\n"
                    + "                            <a class=\"dropdown-item\" href=\"cerrar.htm\">Cerrar Sesión</a>\n"
                    + "                        </div>\n"
                    + "                    </div>\n"
                    + "                </li>";
        }
        modelo.addObject("info", query);

        modelo.addObject("menu", menu);
        return modelo;
    }

}
