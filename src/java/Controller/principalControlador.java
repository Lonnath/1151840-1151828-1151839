/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Config.Conexion;
import Entidad.Cuenta;
import Entidad.Libro;
import Entidad.Persona;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class principalControlador {

    Conexion conexion = new Conexion();
    JdbcTemplate jdbcTemplate = new JdbcTemplate(conexion.connect());
    ModelAndView mav = new ModelAndView();
    Cuenta cuentaTemp = null;

    @RequestMapping("principal.htm")
    public ModelAndView principal() {
        ModelAndView modelo = new ModelAndView("principal");
        String menu = "<li class=\"nav-item dropdown tamanio-drop\">\n"
                + "                    <a class=\"nav-link\" href=\"#\" id=\"navbarDropdownMenuLink\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n"
                + "                        <div class=\"text-title dropdown-toggle navi font-cairo\">Iniciar Sesión</div>\n"
                + "                    </a>\n"
                + "                    <div class=\"drop text-center tamanio-drop\">\n"
                + "                        <div class=\"dropdown-menu tamanio-drop text-center shadow\" aria-labelledby=\"navbarDropdownMenuLink\">\n"
                + "                            <form class=\"tamanio-drop\" method=\"POST\">\n"
                + "                                <div class=\"form-group\">\n"
                + "                                    <label for=\"inputUser\" class=\"font-exo2\">Nombre de Usuario</label>\n"
                + "                                    <div class=\"dropdown-divider\"></div>\n"
                + "                                    <input type=\"text\" class=\"form-control tamanio-text-box-login m-4\" id=\"inputUser\" name=\"nombreUsuario\" aria-describedby=\"emailHelp\" placeholder=\"Ingresar Username\">\n"
                + "                                </div>\n"
                + "                                <div class=\"form-group\">\n"
                + "                                    <label for=\"inputPassword\" class=\"font-exo2\">Contraseña</label>\n"
                + "                                    <div class=\"dropdown-divider\"></div>\n"
                + "                                    <input type=\"password\" class=\"form-control tamanio-text-box-login m-4\" name=\"contrasenia\" placeholder=\"********\">\n"
                + "                                    <small id=\"emailHelp\" class=\"form-text text-muted\">Nuestro equipo nunca le solicitará información personal, recuerde proteger su información.</small>\n"
                + "                                </div>\n"
                + "                                <div class=\"form-group form-check\">\n"
                + "                                    <input type=\"checkbox\" class=\"form-check-input\" id=\"checkPermanecer\" name=\"checkPermanecer\">\n"
                + "                                    <label class=\"form-check-label\" for=\"exampleCheck1\">Permanecer conectado</label>\n"
                + "                                </div>\n"
                + "                                <button type=\"submit\" class=\"btn btn-primary \" value=\"login1\">INGRESAR</button>\n"
                + "                                <a type=\"button\" class=\"btn btn-secondary\" href=\"registrar.htm\">\n"
                + "                                    REGISTRAR\n"
                + "                                </a>\n"
                + "                            </form>\n"
                + "                        </div>\n"
                + "\n"
                + "                    </div>\n"
                + "                </li>";

        if (this.cuentaTemp != null) {
            menu = "<li class=\"nav-item dropdown tamanio-drop\">\n"
                    + "                    <a class=\"nav-link\" href=\"#\" id=\"navbarDropdownMenuLink\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n"
                    + "                        <div class=\"text-title dropdown-toggle navi font-cairo\">Cuenta</div>\n"
                    + "                    </a>\n"
                    + "                    <div class=\"drop text-center tamanio-drop\">\n"
                    + "                        <div class=\"dropdown-menu tamanio-drop\" aria-labelledby=\"navbarDropdownMenuLink\">\n"
                    + "                            <a class=\"dropdown-item\" href=\"perfil.htm\">Perfil</a>\n"
                    + "                            <a class=\"dropdown-item\" href=\"carrito.htm\">Carrito</a>\n"
                    + "                            <a class=\"dropdown-item\" href=\"config.htm\">Configuración</a>\n"
                    + "                            <div class=\"dropdown-divider\"></div>\n"
                    + "                            <a class=\"dropdown-item\" href=\"cerrar.htm\">Cerrar Sesión</a>\n"
                    + "                        </div>\n"
                    + "                    </div>\n"
                    + "                </li>";
        }
        modelo.addObject("menu", menu);
        return modelo;
    }

    

}
