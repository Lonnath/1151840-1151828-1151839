
package Entidad;

public class Cuenta {
    private String nombreUsuario;
    private String contrasenia;

    public Cuenta(){
        this.nombreUsuario=null;
        this.contrasenia=null;
        
    }
    public Cuenta(String userName, String password){
        this.nombreUsuario = userName;
        this.contrasenia = password;
    }
    public String getNombreUsuario(){
        return this.nombreUsuario;
    }
    public void setNombreUsuario(String newName){
        this.nombreUsuario = newName;
    }
    
    public String getContrasenia(){
        return this.contrasenia;
    }
    public void setContrasenia(String newPass){
        this.contrasenia = newPass;
    }
     
}
