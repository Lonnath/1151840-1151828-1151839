package Entidad;

public class Filtros {
    private boolean oferta;
    private boolean solicitud;
    private boolean aventuras;
    private boolean sciFi;
    private boolean autoFiccion;
    private boolean policiaca;
    private boolean filosofia;
    private boolean lengua;
    private boolean geografia;
    private boolean sociales;
    private boolean puras;
    private boolean naturales;

    public boolean isOferta() {
        return oferta;
    }

    public void setOferta(boolean oferta) {
        this.oferta = oferta;
    }

    public boolean isSolicitud() {
        return solicitud;
    }

    public void setSolicitud(boolean solicitud) {
        this.solicitud = solicitud;
    }

    public boolean isAventuras() {
        return aventuras;
    }

    public void setAventuras(boolean aventuras) {
        this.aventuras = aventuras;
    }

    public boolean isSciFi() {
        return sciFi;
    }

    public void setSciFi(boolean sciFi) {
        this.sciFi = sciFi;
    }

    public boolean isAutoFiccion() {
        return autoFiccion;
    }

    public void setAutoFiccion(boolean autoFiccion) {
        this.autoFiccion = autoFiccion;
    }

    public boolean isPoliciaca() {
        return policiaca;
    }

    public void setPoliciaca(boolean policiaca) {
        this.policiaca = policiaca;
    }

    public boolean isFilosofia() {
        return filosofia;
    }

    public void setFilosofia(boolean filosofia) {
        this.filosofia = filosofia;
    }

    public boolean isLengua() {
        return lengua;
    }

    public void setLengua(boolean lengua) {
        this.lengua = lengua;
    }

    public boolean isGeografia() {
        return geografia;
    }

    public void setGeografia(boolean geografia) {
        this.geografia = geografia;
    }

    public boolean isSociales() {
        return sociales;
    }

    public void setSociales(boolean sociales) {
        this.sociales = sociales;
    }

    public boolean isPuras() {
        return puras;
    }

    public void setPuras(boolean puras) {
        this.puras = puras;
    }

    public boolean isNaturales() {
        return naturales;
    }

    public void setNaturales(boolean naturales) {
        this.naturales = naturales;
    }
}
