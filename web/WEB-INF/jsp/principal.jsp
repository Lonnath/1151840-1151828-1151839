<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!doctype html>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css"/>
        <link rel="stylesheet" href="css/personalized.css" type="text/css"/>
        <link rel="stylesheet" href="css/typography.css" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro|Cairo|Roboto|Exo+2|Orbitron|Philosopher&display=swap" rel="stylesheet">
        <link rel="icon" href="img/icon.png"/>

        <title>Tienda Virtual de Libros</title>
            <script type="text/javascript" src="js/mod.js"></script>
    </head>
    <body><!--bgcolor="#F2F0EF"-->
        <!--Barra de menú principal-->
    <nav class="shadow-sm p-3 mb-5 rounded navbar navbar-expand-lg navbar-dark text-title fondo-banner">
        <a class="navbar-brand" href="#"><div class="text-title font-roboto ml-4">Lumier Shop</div></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <div class=""><span class="navbar-toggler-icon"></span></div>
        </button>
        <div class="collapse navbar-collapse content" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="index.htm">
                       <div class="text-title"><img class="img" src="img/iconic/png/home-2x.png"></div><span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="Estanteria.htm">
                        <div class="text-title navi font-cairo">Estanteria</div>
                    </a>
                </li>
                ${menu}
            </ul>
        </div>
    </nav>
    
<!--Contenido-->
<div class="col-md-12 shadow bg-white">
    <div class="row">
        <div class="col-md-5 bg-white p-3 m-2">
            <div id="carouselExampleInterval" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active" data-interval="5000">
                        <img src="img/libros.jpg" class="d-block w-100 img-carrousel" alt="...">
                    </div>
                    <div class="carousel-item" data-interval="10000">
                        <img src="img/libros1.jpg" class="d-block w-100 img-carrousel" alt="...">
                    </div>
                    <div class="carousel-item" data-interval="15000">
                        <img src="img/libros2.jpg" class="d-block w-100 img-carrousel" alt="...">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <div class="col-md-6 bg-white border-left p-3">
            <a href="https://www.eltiempo.com/cultura/musica-y-libros/grandes-entrevistas-con-grandes-artistas-y-otras-figuras-del-arte-436662" class="ofertas"><span class="d-block p-2  fondo-noticia nav nav-tabs"><div class="font-weight-bold">Las bocas del arte: recopilación de entrevistas con grandes personajes.</div><br>Este es un libro de revelaciones e iluminaciones. Dalí dijo que detestaba a los animales domésticos: perros, gatos y niños. Fernando Botero nos contó que cuando apenas tenía veinte años viajó de París a Roma en una vespa y que en esa misma moto destartalada, al mejor estilo del Che Guevara en ‘Los diarios de la motocicleta’, recorrió toda...</span></a>
            <a href="https://www.eltiempo.com/cultura/musica-y-libros/anuncian-ganadores-del-premio-herralde-de-novela-y-del-goncourt-430392" class="ofertas"><span class="d-block p-2  fondo-noticia nav nav-tabs"><div class="font-weight-bold">Anuncian ganadores del Premio Herralde de Novela y del Goncourt.</div><br>Este lunes se anunciaron dos de los premios literarios europeos más esperados cada año: el Herralde de Novela, en España, y el Gouncourt, el máximo galardón de las letras francesas.</span></a>
            <a href="https://www.eltiempo.com/cultura/musica-y-libros/entrega-de-la-decimotercer-version-del-premio-de-literatura-el-barco-de-vapor-425068" class="ofertas"><span class="d-block p-2  fondo-noticia nav nav-tabs"><div class="font-weight-bold">Se realiza nueva versión del premio de literatura El Barco de Vapor.</div><br>Los talentos de la literatura infantil y juvenil colombiana se darán cita el martes en la entrega de la decimosegunda versión del Premio de Literatura Infantil y Juvenil El Barco de Vapor. (Continuar Leyendo...)</span></a>
        </div>
        <br>
    </div>
</div>
<br><br>

<div class="col-md-12">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-4">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-white">
                    <li class="breadcrumb-item active" aria-current="page">Ofertas</li>
                </ol>
            </nav>
        </div>
    </div>    <div class="row">
        <div class="col-md-1"></div>

        <div class="col-md-2 text-center shadow-sm p-3 mb-5 bg-white rounded">
            <a href="#"><img src="img/recurso.jpg" class="img-oferta"></a>
            <br><a href="#" class="ofertas">Titulo:-----<br>Autor:-----<br>Descripción:-----<br>Precio:-----</a><br><br><br>
        </div>

        <div class="col-md-2 text-center shadow-sm p-3 mb-5 bg-white rounded">
            <a href="#"><img src="img/recurso1.jpg" class="img-oferta"></a>
            <br><a href="#" class="ofertas">Titulo:-----<br>Autor:-----<br>Descripción:-----<br>Precio:-----</a><br><br><br>
        </div>


        <div class="col-md-2 text-center shadow-sm p-3 mb-5 bg-white rounded">
            <a href="#"><img src="img/recurso2.jpg" class="img-oferta"></a>
            <br><a href="#" class="ofertas">Titulo:-----<br>Autor:-----<br>Descripción:-----<br>Precio:-----</a><br><br><br>
        </div>


        <div class="col-md-2 text-center shadow-sm p-3 mb-5 bg-white rounded">
            <a href="#"><img src="img/recurso3.jpg" class="img-oferta"></a>
            <br><a href="#" class="ofertas">Titulo:-----<br>Autor:-----<br>Descripción:-----<br>Precio:-----</a><br><br><br>
        </div>


        <div class="col-md-2 text-center shadow-sm p-3 mb-5 bg-white rounded">
            <br><br>
            <a href="Estanteria.htm"><h1><span class="badge badge-secondary color-contenido mt-5">Ver Mas</span></h1></a>
        </div>

    </div>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-4">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-white">
                    <li class="breadcrumb-item active" aria-current="page">Intercambios</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-md-1"></div>

        <div class="col-md-2 text-center shadow-sm p-3 mb-5 bg-white rounded">
            <a href="#"><img src="img/recurso.jpg" class="img-oferta"></a>
            <br><a href="#" class="ofertas">Titulo:-----<br>Autor:-----<br>Descripción:-----<br>Precio:-----</a><br><br><br>
        </div>

        <div class="col-md-2 text-center shadow-sm p-3 mb-5 bg-white rounded">
            <a href="#"><img src="img/recurso1.jpg" class="img-oferta"></a>
            <br><a href="#" class="ofertas">Titulo:-----<br>Autor:-----<br>Descripción:-----<br>Precio:-----</a><br><br><br>
        </div>


        <div class="col-md-2 text-center shadow-sm p-3 mb-5 bg-white rounded">
            <a href="#"><img src="img/recurso2.jpg" class="img-oferta"></a>
            <br><a href="#" class="ofertas">Titulo:-----<br>Autor:-----<br>Descripción:-----<br>Precio:-----</a><br><br><br>
        </div>


        <div class="col-md-2 text-center shadow-sm p-3 mb-5 bg-white rounded">
            <a href="#"><img src="img/recurso3.jpg" class="img-oferta"></a>
            <br><a href="#" class="ofertas">Titulo:-----<br>Autor:-----<br>Descripción:-----<br>Precio:-----</a><br><br><br>
        </div>


        <div class="col-md-2 text-center shadow-sm p-3 mb-5 bg-white rounded">
            <br><br>
            <a href="Estanteria.htm"><h1><span class="badge badge-secondary color-contenido mt-5">Ver Mas</span></h1></a>
        </div>

    </div>
   <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-4">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-white">
                    <li class="breadcrumb-item active" aria-current="page">Mas Vistos</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-md-1"></div>

        <div class="col-md-2 text-center shadow-sm p-3 mb-5 bg-white rounded">
            <a href="#"><img src="img/recurso.jpg" class="img-oferta"></a>
            <br><a href="#" class="ofertas">Titulo:-----<br>Autor:-----<br>Descripción:-----<br>Precio:-----</a><br><br><br>
        </div>

        <div class="col-md-2 text-center shadow-sm p-3 mb-5 bg-white rounded">
            <a href="#"><img src="img/recurso1.jpg" class="img-oferta"></a>
            <br><a href="#" class="ofertas">Titulo:-----<br>Autor:-----<br>Descripción:-----<br>Precio:-----</a><br><br><br>
        </div>


        <div class="col-md-2 text-center shadow-sm p-3 mb-5 bg-white rounded">
            <a href="#"><img src="img/recurso2.jpg" class="img-oferta"></a>
            <br><a href="#" class="ofertas">Titulo:-----<br>Autor:-----<br>Descripción:-----<br>Precio:-----</a><br><br><br>
        </div>


        <div class="col-md-2 text-center shadow-sm p-3 mb-5 bg-white rounded">
            <a href="#"><img src="img/recurso3.jpg" class="img-oferta"></a>
            <br><a href="#" class="ofertas">Titulo:-----<br>Autor:-----<br>Descripción:-----<br>Precio:-----</a><br><br><br>
        </div>


        <div class="col-md-2 text-center shadow-sm p-3 mb-5 bg-white rounded">
            <br><br>
            <a href="Estanteria.htm"><h1><span class="badge badge-secondary color-contenido mt-5">Ver Mas</span></h1></a>
        </div>

    </div>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-4">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-white">
                    <li class="breadcrumb-item active" aria-current="page">Te Podria Interesar</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-md-1"></div>

        <div class="col-md-2 text-center shadow-sm p-3 mb-5 bg-white rounded">
            <a href="#"><img src="img/recurso.jpg" class="img-oferta"></a>
            <br><a href="#" class="ofertas">Titulo:-----<br>Autor:-----<br>Descripción:-----<br>Precio:-----</a><br><br><br>
        </div>

        <div class="col-md-2 text-center shadow-sm p-3 mb-5 bg-white rounded">
            <a href="#"><img src="img/recurso1.jpg" class="img-oferta"></a>
            <br><a href="#" class="ofertas">Titulo:-----<br>Autor:-----<br>Descripción:-----<br>Precio:-----</a><br><br><br>
        </div>


        <div class="col-md-2 text-center shadow-sm p-3 mb-5 bg-white rounded">
            <a href="#"><img src="img/recurso2.jpg" class="img-oferta"></a>
            <br><a href="#" class="ofertas">Titulo:-----<br>Autor:-----<br>Descripción:-----<br>Precio:-----</a><br><br><br>
        </div>


        <div class="col-md-2 text-center shadow-sm p-3 mb-5 bg-white rounded">
            <a href="#"><img src="img/recurso3.jpg" class="img-oferta"></a>
            <br><a href="#" class="ofertas">Titulo:-----<br>Autor:-----<br>Descripción:-----<br>Precio:-----</a><br><br><br>
        </div>


        <div class="col-md-2 text-center shadow-sm p-3 mb-5 bg-white rounded">
            <br><br>
            <a href="Estanteria.htm"><h1><span class="badge badge-secondary color-contenido mt-5">Ver Mas</span></h1></a>
        </div>

    </div>
</div>
<!-- Footer -->
<footer class="page-footer font-small teal pt-4 fondo-footer">

    <!-- Footer Text -->
    <div class="container-fluid text-center text-md-left">

        <!-- Grid row -->
        <div class="row">

            <!-- Grid column -->
            <div class="col-md-6 mt-md-0 mt-3">

                <!-- Content -->
                <h5 class="text-uppercase font-weight-bold nav nav-tabs color-titulo-footer">Lumier Shop</h5>
                <br>
                <div class="col-md-8">
                    <div class="row"><a href="#" class="color-letra-footer">Acerca de Lumier Shop</a></div>
                    <div class="row"><a href="#" class="color-letra-footer">Trabaja con nosotros</a></div>
                    <div class="row"><a href="#" class="color-letra-footer">Politicas y privacidad</a></div>
                </div>
            </div>
            <!-- Grid column -->

            <hr class="clearfix w-100 d-md-none pb-3">

            <!-- Grid column -->
            <div class="col-md-6 mb-md-0 mb-3">

                <!-- Content -->
                <h5 class="text-uppercase font-weight-bold nav nav-tabs color-titulo-footer">Soporte Tecnico</h5>
                <br>
                <div class="col-md-8">
                    <div class="row"><a href="#" class="color-letra-footer">Devoluciones</a></div>
                    <div class="row"><a href="#" class="color-letra-footer">Reembolsos</a></div>
                    <div class="row"><a href="#" class="color-letra-footer">Ayuda para compras</a></div>
                </div>


            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row -->

    </div>
    <!-- Footer Text -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© 2018 Copyright:
        <a href="https://mdbootstrap.com/education/bootstrap/"> MDBootstrap.com</a>
    </div>
    <!-- Copyright -->

</footer>
<!-- Footer -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>
</html>
