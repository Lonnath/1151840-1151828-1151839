<%-- 
    Document   : registrar
    Created on : 11-dic-2019, 23:58:54
    Author     : Luis Mejias
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css"/>
        <link rel="stylesheet" href="css/personalized.css" type="text/css"/>
        <link rel="stylesheet" href="css/typography.css" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro|Cairo|Roboto|Exo+2|Orbitron|Philosopher&display=swap" rel="stylesheet">
        <link rel="shorcut icon" href="img/icon.png"/>

        <title>Tienda Virtual de Libros</title>
            <script type="text/javascript" src="js/mod.js"></script>
    </head>
    <body><!--bgcolor="#F2F0EF"-->
        <!--Barra de menú principal-->
    <nav class="shadow-sm p-3 mb-5 rounded navbar navbar-expand-lg navbar-dark text-title fondo-banner">
        <a class="navbar-brand" href="#"><div class="text-title font-roboto ml-4">Lumier Shop</div></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <div class=""><span class="navbar-toggler-icon"></span></div>
        </button>
        <div class="collapse navbar-collapse content" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="index.htm">
                        <div class="text-title"><img class="img" src="img/iconic/png/home-2x.png"></div><span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="Estanteria.htm">
                        <div class="text-title navi font-cairo">Estanteria</div>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="index.htm">
                        <div class="text-title navi font-cairo">Iniciar Sesión</div>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    
                
               
<!--Contenido-->
                    <form class="needs-validation" novalidate method="POST">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="nombre">Nombres</label>
                                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombres" required>
                                <div class="invalid-feedback">
                                        Por favor, llene este campo.
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="correo">Correo</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroupPrepend">@</span>
                                    </div>
                                    <input type="text" class="form-control" id="correo" name="correo" placeholder="Ejemplo123" aria-describedby="inputGroupPrepend" required>
                                    <div class="invalid-feedback">
                                        Por favor, Digite un Correo Valido.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="username">Identificador:</label>
                                <input type="text" class="form-control" id="username" name="username" placeholder="Username" required>
                                <div class="invalid-feedback">
                                    Por favor, Digite una Nombre de Usuario Valido.
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="contrasenia">Contraseña</label>
                                <input type="password" class="form-control" id="contrasenia" name="contrasenia" placeholder="*******" required>
                                <div class="invalid-feedback">
                                    Por favor, Digite una Contraseña Valida.
                                </div>
                            </div>


                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="ciudad">Ciudad</label>
                                <input type="text" class="form-control" id="ciudad" name="ciudad" placeholder="Ciudad" required>
                                <div class="invalid-feedback">
                                    Por favor, Digite una Ciudad Valida.
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="departamento">Estado/Departamento</label>
                                <input type="text" class="form-control" id="departamento" name="departamento" placeholder="Estado/Departamento" required>
                                <div class="invalid-feedback">
                                    Por favor, Digite un Estado Valido.
                                </div>
                            </div>


                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="documento">Documento de Identidad</label>
                                <input type="text" class="form-control" id="documento" name="documento" placeholder="Doc. de Identidad" required>
                                <div class="invalid-feedback">
                                    Por favor, Digite un Documento Valido.
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="fecha">Fecha de Nacimiento</label>
                                <input type="text" class="form-control" id="fecha" name="fecha" placeholder="Año-Mes-Dia" required>
                                <div class="invalid-feedback">
                                    Por favor, Digite una Fecha Valida.
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="tarjeta">No. Tarjeta de Credito</label>
                                <input type="text" class="form-control" id="tarjeta" name="tarjeta" placeholder="0000 0000 0000 0000" required>
                                <div class="invalid-feedback">
                                    Por favor, Digite una Tarjeta Valida.
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="fechaExpiracionTarjeta">Fecha de Expiración</label>
                                <input type="text" class="form-control" id="fechaExpiracionTarjeta" name="fechaExpiracionTarjeta" placeholder="Año-Mes-Dia" required>
                                <div class="invalid-feedback">
                                    Por favor, Digite una Fecha Valida.
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
                                <label class="form-check-label" for="invalidCheck">
                                    Aceptar Terminos y Condiciones
                                </label>
                                <div class="invalid-feedback">
                                    Debes aceptar los terminos y condiciones antes de continuar.
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                             <button type="submit" class="btn btn-primary" value="registrarUsuario">CONFIRMAR</button>
                        </div>
                    </form>
<!-- Footer -->
<footer class="page-footer font-small teal pt-4 fondo-footer">

    <!-- Footer Text -->
    <div class="container-fluid text-center text-md-left">

        <!-- Grid row -->
        <div class="row">

            <!-- Grid column -->
            <div class="col-md-6 mt-md-0 mt-3">

                <!-- Content -->
                <h5 class="text-uppercase font-weight-bold nav nav-tabs color-titulo-footer">Lumier Shop</h5>
                <br>
                <div class="col-md-8">
                    <div class="row"><a href="#" class="color-letra-footer">Acerca de Lumier Shop</a></div>
                    <div class="row"><a href="#" class="color-letra-footer">Trabaja con nosotros</a></div>
                    <div class="row"><a href="#" class="color-letra-footer">Politicas y privacidad</a></div>
                </div>
            </div>
            <!-- Grid column -->

            <hr class="clearfix w-100 d-md-none pb-3">

            <!-- Grid column -->
            <div class="col-md-6 mb-md-0 mb-3">

                <!-- Content -->
                <h5 class="text-uppercase font-weight-bold nav nav-tabs color-titulo-footer">Soporte Tecnico</h5>
                <br>
                <div class="col-md-8">
                    <div class="row"><a href="#" class="color-letra-footer">Devoluciones</a></div>
                    <div class="row"><a href="#" class="color-letra-footer">Reembolsos</a></div>
                    <div class="row"><a href="#" class="color-letra-footer">Ayuda para compras</a></div>
                </div>


            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row -->

    </div>
    <!-- Footer Text -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© 2018 Copyright:
        <a href="https://mdbootstrap.com/education/bootstrap/"> MDBootstrap.com</a>
    </div>
    <!-- Copyright -->

</footer>
<!-- Footer -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>
</html>