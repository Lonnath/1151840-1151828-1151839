-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-12-2019 a las 07:53:15
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `lumier_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `id` int(11) NOT NULL,
  `titulo` varchar(40) NOT NULL,
  `autor` varchar(25) NOT NULL,
  `anio_publicacion` date NOT NULL,
  `precio` int(11) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `imgDir` varchar(100) NOT NULL,
  `genero` varchar(15) NOT NULL,
  `propietario` int(11) NOT NULL,
  `realizada` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ofertas`
--

CREATE TABLE `ofertas` (
  `id` int(11) NOT NULL,
  `titulo` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `autor` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `anio_publicacion` date NOT NULL,
  `precio` int(11) NOT NULL,
  `descripcion` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `imgDir` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `genero` int(15) NOT NULL,
  `propietario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `ofertas`
--

INSERT INTO `ofertas` (`id`, `titulo`, `autor`, `anio_publicacion`, `precio`, `descripcion`, `imgDir`, `genero`, `propietario`) VALUES
(1, 'Chespirito', 'El chavito', '1998-12-12', 1234554, '13ldskvjuoh K POKJ 32J R3IM 4tm omgpjpgijw', '   ', 0, 0),
(2, 'La llorona', 'La que llora', '2001-07-26', 1234567890, 'una mujer que llora por los ojos', '   ', 0, 0),
(3, 'Chespirito', '12313', '1998-12-12', 999, 'lll', '   ', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE `personas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `documento_identidad` int(11) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `correo` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `departamento` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `ciudad` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `tarjeta_credito` int(18) NOT NULL,
  `fecha_expiracion` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `personas`
--

INSERT INTO `personas` (`id`, `nombre`, `documento_identidad`, `fecha_nacimiento`, `correo`, `departamento`, `ciudad`, `tarjeta_credito`, `fecha_expiracion`) VALUES
(1, '112312', 12312312, '1991-12-12', '12213', '123123', '123123', 1231231312, '1998-12-12'),
(2, '1231', 1321321, '1991-12-12', '1312', '12312', '13213', 123123, '1998-12-12'),
(4, 'LLL', 12313123, '1991-12-12', 'LUIOK@gmail.co', 'Guadalajara', 'Chinacota', 123, '1998-12-12'),
(5, 'efef', 2112, '1991-12-12', 'jijiji@hotmail.UK', 'guadalajara', 'chinacota', 12312314, '1998-12-12'),
(6, 'Kare ', 1232134, '1991-12-12', 'jijiji@hotmail.UK', 'Guadalajara', 'Chinacota', 1231456, '1998-12-12'),
(8, 'Ovallos', 1111111, '1991-12-12', 'caremojica@jjj.co', '112312', 'kiosko', 123123, '1998-12-12'),
(9, 'dwdw2', 12412222, '1991-12-12', 'jijiji@hotmail.UK', 'Guadalajara', 'Chinacota', 123456789, '1998-12-12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitudes`
--

CREATE TABLE `solicitudes` (
  `id` int(11) NOT NULL,
  `titulo` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `autor` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `anio_publicacion` date NOT NULL,
  `genero` int(15) NOT NULL,
  `propietario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `usuario` varchar(12) COLLATE utf8_spanish_ci NOT NULL,
  `contrasenia` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `propietario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `contrasenia`, `propietario`) VALUES
(1, '123123', '12312', 12312312),
(2, '123231', '13123', 1321321),
(3, 'userName', '12321312', 12313123),
(4, 'motherrussia', 'rgrgeg', 2112),
(5, 'MotherRussia', '13214', 1232134),
(6, 'elCajonero', '123123', 1111111),
(7, 'wdwhrg', 'grege4', 12412222);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ofertas`
--
ALTER TABLE `ofertas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `documento_identidad` (`documento_identidad`);

--
-- Indices de la tabla `solicitudes`
--
ALTER TABLE `solicitudes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `compras`
--
ALTER TABLE `compras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ofertas`
--
ALTER TABLE `ofertas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `personas`
--
ALTER TABLE `personas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `solicitudes`
--
ALTER TABLE `solicitudes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
